#
# root/Makefile
#
# This file is part of fuzz2000
# Copyright (c) 1982--2006 J. M. Spivey
# Copyright (c) 2011 Michele Bini <michele.bini@gmail.com>
# All rights reserved
#
# Modified by Michele Bini <michele.bini@gmail.com> 2011-11
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# $Id: Makefile,v 1.3 2007-02-16 12:45:13 mike Exp $
#

# Edit these directories to suit your setup
BINDIR = /usr/bin
LIBDIR = /usr/lib
TEXDIR = /usr/share/texmf/tex/latex/zfuzz
TEX209DIR = /usr/share/texmf/tex/latex209
MFDIR = /usr/share/texmf/fonts/source/zfuzz

INSTALL = install

DESTDIR =

all: src doc

test: force
	$(MAKE) -C test test

install: src
	test -e $(DESTDIR) && $(INSTALL) -d $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 src/neofuzz $(DESTDIR)$(BINDIR)
	test -e $(DESTDIR) && $(INSTALL) -d $(DESTDIR)$(LIBDIR)
	$(INSTALL) -m 644 src/neofuzzlib $(DESTDIR)$(LIBDIR)/neofuzzlib
	test -e $(DESTDIR) && $(INSTALL) -d $(DESTDIR)$(TEXDIR)
	$(INSTALL) -m 644 tex/fuzz.sty $(DESTDIR)$(TEXDIR)
	test -e $(DESTDIR) && $(INSTALL) -d $(DESTDIR)$(TEX209DIR)
	test -e $(DESTDIR) && $(INSTALL) -m 644 doc/fuzz209.sty $(DESTDIR)$(TEX209DIR)/fuzz.sty
	test -e $(DESTDIR) && $(INSTALL) -d $(DESTDIR)$(MFDIR)
	$(INSTALL) -m 644 tex/*.mf $(DESTDIR)$(MFDIR)

SUBDIRS = src doc

src: force
	$(MAKE) -C $@ all

doc: force
	sh -c ./debian/docbuildrecipe.sh

clean distclean realclean: force
	for d in $(SUBDIRS); do make -C $$d $@; done

force:
